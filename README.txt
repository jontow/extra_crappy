2021-07-24
Jonathan Towne <jontow@mototowne.com>

Original code based on the example game "Flappy Dragon" (Chapter 3) in the
book "Hands On Rust", by Herbert Wolverson; published by The Pragmatic
Programmers.

Currently unclear on the license of that original code, but this repository
holds amusement value only, not monetary value.

This version is heavily modified, with modified physics, rockets, and such
other niceties as I remember to document here.
