use bracket_lib::prelude::*;

#[derive(Debug)]
enum GameMode {
    Menu,
    Playing,
    End,
}

const SCREEN_WIDTH : i32 = 80;
const SCREEN_HEIGHT : i32 = 50;

// Base game speed:
const FRAME_DURATION : f32 = 75.0;
// How much faster game becomes when player levels up:
const GAME_SPEEDUP : f32 = 2.0;

const FLAP_HEIGHT : f32 = -2.0;
const GRAVITY_PULL : f32 = 2.0;

// Number of points required to level up:
const LEVEL_UP_THRESHOLD : i32 = 10;

// Rocket range limited: if true, loses altitude over distance
//   Note: currently buggy but usable, if a little unpredictable.
const ROCKET_RANGE_LIMITED : bool = false;
// Rocket advancement (x axis) per tick:
const ROCKET_SPEED : i32 = 2;
// Number of vertical 'chunks' to remove from obstacle:
const ROCKET_EXPLOSION_SIZE : i32 = 3;

#[derive(Debug)]
struct Player {
    x: i32,
    y: i32,
    velocity: f32,
    rocket_x: i32,
    rocket_y: i32,
    score: i32,
    level: i32,
}

impl Player {
    fn new(x: i32, y: i32) -> Self {
        Player {
            x,
            y,
            velocity: 0.0,
            rocket_x: -1,
            rocket_y: -1,
            score: 0,
            level: 1,
        }
    }

    fn render(&mut self, ctx: &mut BTerm) {
        ctx.set(
            0,
            self.y,
            YELLOW,
            BLACK,
            to_cp437('@')
        );

        if self.rocket_y >= 0 {
            ctx.set(
                self.rocket_x,
                self.rocket_y,
                RED,
                BLACK,
                to_cp437('=')
            );
        }
    }

    fn flap(&mut self) {
        self.velocity = FLAP_HEIGHT;
    }

    fn fire(&mut self) {
        if self.rocket_x > 0 {
            false;
        } else {
            self.rocket_x = 1;
            self.rocket_y = self.y;
        }
        println!("DEBUG: fire() rocket: rocket_x={}, rocket_y={}", self.rocket_x, self.rocket_y);
    }

    fn gravity_and_move(&mut self) {
        if self.velocity < GRAVITY_PULL {
            self.velocity += 0.2;
        }
        self.y += self.velocity as i32;
        self.x += 1;
        if self.y < 0 {
            self.y = 0;
        }

        // Rocket fired and making progress
        if self.rocket_y >= 0 {
            self.rocket_x += ROCKET_SPEED;

            if ROCKET_RANGE_LIMITED {
                // Rocket loses altitude quickly after ~50% of screen width
                if self.rocket_x >= (SCREEN_WIDTH / 2) {
                    self.rocket_y += 2;
                    // Begin rocket losing altitude after ~33% of screen width
                } else if self.rocket_x >= (SCREEN_WIDTH / 3) {
                    self.rocket_y += 1;
                }
            }
        }
    }
}

#[derive(Debug)]
struct State {
    player: Player,
    frame_time: f32,
    obstacle: Obstacle,
    mode: GameMode,
    game_speed: f32,
}

impl State {
    fn new() -> Self {
        State {
            player: Player::new(5, 25),
            frame_time: 0.0,
            game_speed: FRAME_DURATION,
            obstacle: Obstacle::new(SCREEN_WIDTH, 0),
            mode: GameMode::Menu,
        }
    }

    fn dead(&mut self, ctx: &mut BTerm) {
        ctx.cls();
        ctx.print_centered(5, "Congratulations!  You are dead!");
        ctx.print_centered(6, &format!("You attained level {} and earned {} points!", self.player.level, self.player.score));
        ctx.print_centered(8, "(P) Play Again");
        ctx.print_centered(9, "(Q) Quit Game");

        if let Some(key) = ctx.key {
            match key {
                VirtualKeyCode::P => self.restart(),
                VirtualKeyCode::Q => ctx.quitting = true,
                _ => {}
            }
        }
    }

    fn main_menu(&mut self, ctx: &mut BTerm) {
        ctx.cls();
        ctx.print_centered(5, "Welcome to Crappy Dragon");
        ctx.print_centered(8, "(P) Play Game");
        ctx.print_centered(9, "(Q) Quit Game");

        if let Some(key) = ctx.key {
            match key {
                VirtualKeyCode::P => self.restart(),
                VirtualKeyCode::Q => ctx.quitting = true,
                _ => {}
            }
        }
    }

    fn play(&mut self, ctx: &mut BTerm) {
        ctx.cls_bg(NAVY);
        // game speed: lower number is faster.
        self.game_speed = FRAME_DURATION - (GAME_SPEEDUP * self.player.level as f32);
        self.frame_time += ctx.frame_time_ms;
        if self.frame_time > self.game_speed {
            self.frame_time = 0.0;

            self.player.gravity_and_move();
        }
        if let Some(key) = ctx.key {
            match key {
                VirtualKeyCode::Space => self.player.flap(),
                VirtualKeyCode::F => self.player.fire(),
                _ => {}
            }
        }

        self.player.render(ctx);
        ctx.print(0, 0, "Press SPACE to flap, F to fire rocket.");
        ctx.print(0, 1, &format!("Score: {}     Level: {}", self.player.score, self.player.level));

        self.obstacle.render(ctx, self.player.x);

        // Player has passed the obstacle, incr score and gen new obstacle
        if self.player.x > self.obstacle.x {
            self.player.score += 1;
            self.obstacle = Obstacle::new(
                self.player.x + SCREEN_WIDTH, self.player.score
            );
        }

        // Player scored enough points to level up
        if self.player.score >= (self.player.level * LEVEL_UP_THRESHOLD) {
            self.player.level += 1
        }

        // Player has hit the obstacle (or exceeded SCREEN_HEIGHT)
        if self.player.y > SCREEN_HEIGHT ||
            self.obstacle.hit_obstacle(&self.player)
        {
            self.mode = GameMode::End;
        }

        // Player rocket hit an obstacle, increase score
        if self.obstacle.rocket_obstacle(&mut self.player) {
            self.player.score += 1;
        }
    }

    fn restart(&mut self) {
        self.player = Player::new(5, 25);
        self.frame_time = 0.0;
        self.obstacle = Obstacle::new(SCREEN_WIDTH, 0);
        self.mode = GameMode::Playing;
        self.player.score = 0;
        self.player.level = 1;
    }
}

impl GameState for State {
    fn tick(&mut self, ctx: &mut BTerm) {
        match self.mode {
            GameMode::Menu => self.main_menu(ctx),
            GameMode::Playing => self.play(ctx),
            GameMode::End => self.dead(ctx),
        }
        //ctx.print(1, 1, "Hello, Bracket Terminal!");
        // see also: ctx.print(format!("{}", my_string));
    }
}

#[derive(Debug)]
struct Obstacle {
    x: i32,
    dynamic_y: Vec<i32>,
}

impl Obstacle {
    fn new(x: i32, score: i32) -> Self {
        let mut random = RandomNumberGenerator::new();

        // Start with a few fixed positions at top & bottom of screen:
        let mut prepop_dyn_y = vec![];
        //let mut prepop_dyn_y = vec![0, 1, SCREEN_HEIGHT - 2, SCREEN_HEIGHT - 1];
        // Some fixed-positions for testing:
        //let prepop_dyn_y = vec![0, 1, 7, 8, 9, 10, 11, 12, 13, 14, 15, SCREEN_HEIGHT - 2, SCREEN_HEIGHT - 1];

        // Fill entire dynamic range with screen positions
        for i in 0..SCREEN_HEIGHT {
            prepop_dyn_y.push(i)
        }
        //println!("DEBUG: prepop_dyn_y: {:?}", prepop_dyn_y);

        // Erase a random section, size decreasing as score increases
        let hole_size = i32::max(2, 20 - (score / 2));
        let hole_start = random.range(3, SCREEN_HEIGHT - hole_size);
        println!("DEBUG: generating new hole in obstacle of size {} at start {}", hole_size, hole_start);
        let mut counter = hole_start;
        //println!("DEBUG: outside while counter<hole_size loop");
        while counter <= (hole_start + hole_size) {
            //println!("DEBUG: inside while counter<hole_size loop (counter={})", counter);
            if let Some(pos) = prepop_dyn_y.iter().
                position(|&elem| elem == counter) {
                    //println!("DEBUG: opening hole in obstacle at {}(pos {}) (hole_size {})", prepop_dyn_y[pos], pos, hole_size);
                    prepop_dyn_y.remove(pos);
                    //println!("DEBUG: prepop_dyn_y: {:?}", prepop_dyn_y);
            } else {
                println!("DEBUG: why would I get here? counter={}", counter);
            }
            counter += 1;
        }

        Obstacle {
            x,
            dynamic_y: prepop_dyn_y,
        }
    }

    fn render(&mut self, ctx: &mut BTerm, player_x : i32) {
        let screen_x = self.x - player_x;

        // Draw the dynamic_y style obstacle (experimental)
        for y in self.dynamic_y.clone() {
            ctx.set(
                screen_x,
                y,
                GREEN,
                BLACK,
                to_cp437('|'),
            );
        }
    }

    fn rocket_obstacle(&mut self, player: &mut Player) -> bool {
        let screen_x = self.x - player.x;

        // rocket has run off screen, reset, reload, and ready!
        if player.rocket_x >= SCREEN_WIDTH ||
           player.rocket_y >= SCREEN_HEIGHT ||
           player.rocket_y <= 0 {
            //println!("rocket_x={} (rocket_y = {}, screen_x = {}, player_x = {})", player.rocket_x, player.rocket_y, screen_x, player.x);
            if player.rocket_x != -1 && player.rocket_y != -1 {
                println!("DEBUG: rocket_obstacle(): rocket ran off screen at (x,y):({},{})", player.rocket_x, player.rocket_y);
            }
            player.rocket_x = -1;
            player.rocket_y = -1;
            return false;
        }

        if self.dynamic_y.iter().any(|&i| i==player.rocket_y) {
            println!("rocket_x={} (rocket_y = {}, screen_x = {}, player_x = {})", player.rocket_x, player.rocket_y, screen_x, player.x);
            if player.rocket_x >= screen_x {
                println!("DEBUG: hit at {}!", player.rocket_y);
                //println!("DEBUG: dynamic_y: {:?}", self.dynamic_y);

                // First, remove the direct hit spot
                if let Some(pos) = self.dynamic_y.iter().position(|&elem| elem == player.rocket_y) {
                    //println!("DEBUG: attempting obstacle remove +/- {}", ROCKET_EXPLOSION_SIZE);
                    self.dynamic_y.remove(pos);
                }

                // Second, remove the lower side(less-than values) of the barrier
                let mut counter = 0;
                while (ROCKET_EXPLOSION_SIZE * -1) < counter {
                    //println!("DEBUG: ({}) < counter({}) ... {}", (ROCKET_EXPLOSION_SIZE * -1), counter, (player.rocket_y - counter));
                    if let Some(pos) = self.dynamic_y.iter().
                       position(|&elem| elem == (player.rocket_y + counter)) {
                        //println!("DEBUG: attempting obstacle remove {}(at pos {})", self.dynamic_y[pos], pos);
                        self.dynamic_y.remove(pos);
                    }
                    counter -= 1;
                }

                // Third, remove the upper side(greater-than values) of the barrier
                let mut counter = 0;
                while counter < ROCKET_EXPLOSION_SIZE {
                    //println!("DEBUG: counter({}) < ({}) ... {}", counter, ROCKET_EXPLOSION_SIZE, (player.rocket_y + counter));
                    if let Some(pos) = self.dynamic_y.iter().
                       position(|&elem| elem == (player.rocket_y + counter)) {
                        //println!("DEBUG: attempting obstacle remove {}(at pos {})", self.dynamic_y[pos], pos);
                        self.dynamic_y.remove(pos);
                    }
                    counter += 1;
                }

                // Reset and reload rocket
                player.rocket_y = -1;
                player.rocket_x = -1;
                return true;
            }
            false
        } else {
            // we didn't hit it.
            return false;
        }
    }

    fn hit_obstacle(&self, player: &Player) -> bool {
        let does_x_match = player.x == self.x;
        let mut does_y_match = false;
        if let Some(_pos) = self.dynamic_y.iter().
           position(|&elem| elem == player.y) {
            does_y_match = true;
        }

        does_x_match && does_y_match
    }
}

fn main() -> BError {
    let context = BTermBuilder::simple80x50()
        .with_title("Crappy Dragon")
        .build()?;

    main_loop(context, State::new())
}
